import { getJson } from "./ajax";

export class Menu {
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
        this.body = document.body;
        this.content = document.querySelector('.content');
        this.toggle = document.querySelector('#toggle');
    }

    build() {
        getJson(this.apiUrl)
            .then(JSON.parse)
            .then( (r) => this.prepare(r))
            .then(() => this.append() )
            .then(() => this.bindEvents() );

        return this;
    }
    
    prepare(tree) {
        this.nav = '<nav class="main-nav">';
        this.buildTree(tree)
        this.nav += '</nav>';
    }

    buildTree(tree, klass = '') {
        this.nav += `<ul class="menu-list ${klass}">`;
        let item = null;
        for (item in tree.items ) {
            let link = tree.items[item];
            this.nav += `<li class="menu-item"><a class="menu-link" href="${link.url}">${link.label}</a>`;
            if (link.items && link.items.length) {
                this.buildTree(link, 'menu-list--submenu');
            }
            this.nav += `</li>`;
        }
        this.nav += `</ul>`;
    }

    append() {   
        let header = document.querySelector('.main-header');

        header.insertAdjacentHTML('beforeend', this.nav);
        this.menu = document.querySelector('.main-nav');

        this.body.insertAdjacentHTML('afterbegin', '<div class="nav-mask"></div>');
        this.mask = document.querySelector('.nav-mask');
    }

    close() {
        this.body.classList.remove('has-active-menu');
        this.menu.classList.remove('is-active');
        this.mask.classList.remove('is-active');
        this.content.classList.remove('has-push-right');
    }

    open() {
        this.body.classList.add('has-active-menu');
        this.menu.classList.add('is-active');
        this.mask.classList.add('is-active');
        this.content.classList.add('has-push-right');
    }

    isOpen() {
        return this.body.classList.contains('has-active-menu');
    }

    bindEvents() {
        this.toggle.addEventListener('click', (e) => {
            e.preventDefault();
            console.log('teste');
            if(this.isOpen()) {
                this.close();
            } else {
                this.open();
            }
        });

        this.mask.addEventListener('click', (e) => {
            e.preventDefault(); 
            this.close();
        });
    }
}