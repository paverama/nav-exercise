var gulp   = require("gulp");
var sass   = require('gulp-sass');
var notify = require('gulp-notify');
var rollup = require('gulp-rollup');

gulp.task('sass', function () {
  return gulp.src('./src/scss/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/styles/'));
});

gulp.task('scripts', function () {
  return gulp.src('./src/js/**.*js')
    .pipe(rollup({
      entry: './src/js/main.js'
    }))
    .pipe(notify('Scripts compiled'))
    .pipe(gulp.dest('./public/js/'));
});
 
gulp.task('watch', function() {
    gulp.watch("./src/scss/**/*.scss", ['sass']);
    gulp.watch("./src/js/**/*.js", ['scripts']); 
});

gulp.task('build', ['sass', 'scripts']);

gulp.task('default', [ 'build' ]);